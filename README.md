# layui_qrcode
 <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
 				<legend>layui生成二维码的扩展进阶用法</legend>
 			</fieldset>
 			<p><strong><strong><strong>指定</strong>二维码的生成方式：</strong><br /></strong></p>
 			<p>可以在调用函数的同时输入想要的二维码生成方式（table/canvas）。<br /></p>
 			<pre class="layui-code">//使用table生成
 $('#qrcode').qrcode({
 	render: "table",
 	text: "http://www.buersoft.cn"
 });
 
 //使用canvas生成
 $('#qrcode').qrcode({
 	render: "canvas",
 	text: "http://www.buersoft.cn"
 });</pre>
 			<p><strong><strong>指定</strong>生成二维码的大小：</strong></p>
 			<p>可以在调用函数的同时输入想要生成二维码的宽度和高度即可指定生成的二维码的大小。</p>
 			<pre class="layui-code">//生成100*100(宽度100，高度100)的二维码
 $('#qrcode').qrcode({
 	render: "canvas", //也可以替换为table
 	width: 100,
 	height: 100,
 	text: "http://www.buersoft.cn"
 });</pre>
 			<p><strong><strong>指定</strong>生成二维码的色彩样式：</strong></p>
 			<p>可以在调用函数的同时输入想要生成二维码的前景色和背景色即可指定生成的二维码的色彩样式。</p>
 			<pre class="layui-code">//生成前景色为红色背景色为白色的二维码
 $('#qrcode').qrcode({
 	render: "canvas", //也可以替换为table
 	foreground: "#C00",
 	background: "#FFF",
 	text: "http://www.buersoft.cn"
 });</pre>
 			<p><strong>中文ULR生成方法:</strong></p>
 			<pre class="layui-code"> $("#output").qrcode(encodeURI("http://中文中文"));//使用encodeURI进行转码</pre>
 		</div>
